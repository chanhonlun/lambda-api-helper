import { APIGatewayEvent, Handler as LambdaHandler } from 'aws-lambda';
import { isObjectLike } from 'lodash';

import { HttpContext, Middleware, Next, Request, Response } from '../types';

import { toRequest, toResponse } from './parser';
import { Pipeline, Task } from './pipeline';

export interface HandlerOptions {
  initialize?: () => void;
  middlewares?: Middleware[];
}

export type HandlerFn = (
  request: Request,
  response: Response
) => Promise<any | void>;

const DEFAULT_OPTIONS: HandlerOptions = {
  initialize: () => {},
  middlewares: [],
};

export function extend(
  options: HandlerOptions,
  handler: HandlerFn
): LambdaHandler<APIGatewayEvent> {
  options = Object.assign({}, DEFAULT_OPTIONS, options);

  if (typeof options.initialize === 'function') {
    options.initialize();
  }

  return async (event, context, callback) => {
    const request = toRequest(event, context);
    const response = toResponse(callback);

    const middlewares = [...(options.middlewares ?? [])];
    handler && middlewares.push(toMiddleware(handler));

    const tasks = middlewares.map((middleware) => toPipelineTask(middleware));

    try {
      await new Pipeline<HttpContext>(tasks).execute({ request, response });

      if (!response.sent) {
        response.send(
          isObjectLike(response._body)
            ? JSON.stringify(response._body)
            : response._body
        );
      }
    } catch (error) {
      const { statusCode, message } = error;

      response.status(statusCode ?? 500).send(
        JSON.stringify({
          errors: [{ message: message ?? "something's wrong" }],
        })
      );
    }
  };
}

function toPipelineTask(middleware: Middleware): Task<HttpContext> {
  return ({ request, response }, next: Next) =>
    middleware(request, response, next);
}

function toMiddleware(handler: HandlerFn): Middleware {
  return async (request, response, next) => {
    const result = await handler(request, response);
    if (result !== undefined) {
      response.body(result);
    }
    await next();
  };
}
