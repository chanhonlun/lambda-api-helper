import { Request, Response } from '../types';

export function toRequest(event: any, context: any): Request {
  let body;

  try {
    body = JSON.parse(event.body);
  } catch (e) {
    body = event.body;
  }

  const query = Object.keys(event.queryStringParameters ?? {})
    .map((key) => {
      const isMultiValue =
        event.multiValueQueryStringParameters[key]?.length > 1;
      const value = isMultiValue
        ? event.multiValueQueryStringParameters[key]
        : event.queryStringParameters[key];
      return { [key]: value };
    })
    .reduce(
      (previousValue, currentValue) => ({ ...previousValue, ...currentValue }),
      {}
    );

  return {
    method: event.httpMethod,
    path: event.path,
    body,
    pathParameters: event.pathParameters,
    query,
    headers: event.headers,
    locals: {},
    getHeader: (path: string): string =>
      event.headers[path] ||
      Object.entries(event.headers).find(
        ([key]) => key.toLowerCase() === path.toLowerCase()
      )?.[1],

    event: event,
    context: context,
  };
}

export function toResponse(callback: any): Response {
  return {
    _headers: {},
    _status: 200,
    _body: null,
    sent: false,
    header(key, value) {
      this._headers[key] = value;
      return this;
    },
    status(status) {
      this._status = status;
      return this;
    },
    body(body) {
      this._body = body;
      return this;
    },
    send(body?) {
      this.callback(null, {
        headers:
          Object.keys(this._headers).length > 0 ? this._headers : undefined,
        statusCode: this._status,
        body: body || this._body,
      });
    },

    callback(...args: any[]) {
      if (this.sent) throw new Error('already sent response');

      this.sent = true;
      return callback(...args);
    },
  };
}
