import { Pipeline } from '../pipeline';

describe('pipeline.ts', () => {
  it('runs tasks in order', async () => {
    const arg: { foo: string } = { foo: '' };

    const pipeline = new Pipeline<{ foo: string }>([
      async (arg, next) => {
        arg.foo += 'a';
        await next();
      },
      async (arg, next) => {
        arg.foo += 'b';
        await next();
      },
    ]);

    await pipeline.execute(arg);

    expect(arg.foo).toBe('ab');
  });

  it('stops when not calling next()', async () => {
    const arg: { foo: string } = { foo: '' };

    const pipeline = new Pipeline<{ foo: string }>([
      async (arg, next) => {
        arg.foo += 'a';
      },
      async (arg, next) => {
        arg.foo += 'b';
        await next();
      },
    ]);

    await pipeline.execute(arg);

    expect(arg.foo).toBe('a');
  });

  it('runs codes after next() in reverse order of tasks', async () => {
    const arg: { foo: string } = { foo: '' };

    const pipeline = new Pipeline<{ foo: string }>([
      async (arg, next) => {
        arg.foo += 'a';
        await next();
        arg.foo += 'd';
      },
      async (arg, next) => {
        arg.foo += 'b';
        await next();
        arg.foo += 'c';
      },
    ]);

    await pipeline.execute(arg);

    expect(arg.foo).toBe('abcd');
  });

  it('stops when task throws error', async () => {
    const arg: { foo: string } = { foo: '' };

    const pipeline = new Pipeline<{ foo: string }>([
      async (arg, next) => {
        arg.foo += 'a';
        if (arg.foo === 'a') throw new Error();
        await next();
      },
      async (arg, next) => {
        arg.foo += 'b';
        await next();
      },
    ]);

    await expect(pipeline.execute(arg)).rejects.toThrow();
    expect(arg.foo).toBe('a');
  });
});
