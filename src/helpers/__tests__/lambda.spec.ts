import 'jest-extended';

import * as lambda from '../lambda';

import context from './context.json';
import noBodyEvent from './event.no-body.json';

describe('lambda.ts', () => {
  it('runs initialize() before handler()', async () => {
    const initialize = jest.fn(() => {});

    const handler = lambda.extend({ initialize }, async (request, response) => {
      response.send('hello world');
    });
    const callback = jest.fn((err, data) => {});
    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(initialize).toBeCalled();
    expect(initialize).toBeCalledTimes(1);
    expect(initialize).toHaveBeenCalledBefore(callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('sends response', async () => {
    const handler = lambda.extend({}, async (request, response) => {
      response.send('hello world');
    });
    const callback = jest.fn((err, data) => {});
    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('sends response if set body but not called send()', async () => {
    const handler = lambda.extend({}, async (request, response) => {
      response.body('hello world');
    });
    const callback = jest.fn((err, data) => {});
    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('sends response if returned body but not called send()', async () => {
    const handler = lambda.extend({}, async (request, response) => {
      return 'hello world';
    });
    const callback = jest.fn((err, data) => {});
    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('runs middleware before handler', async () => {
    const handler = lambda.extend(
      {
        middlewares: [
          async (request, response, next) => {
            response.status(201);
            await next();
          },
        ],
      },
      async (request, response) => {
        response.send('hello world');
      }
    );
    const callback = jest.fn((err, data) => {});
    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 201,
      body: 'hello world',
    });
  });

  it('breaks middlewares chain if not called next()', async () => {
    const handler = lambda.extend(
      {
        middlewares: [
          async (request, response, next) => {
            response.status(400).send('invalid input');
          },
        ],
      },
      async (request, response) => {
        response.send('hello world');
      }
    );
    const callback = jest.fn((err, data) => {});
    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 400,
      body: 'invalid input',
    });
  });

  it('does post-works after next() if not called send()', async () => {
    const handler = lambda.extend(
      {
        middlewares: [
          async (request, response, next) => {
            // pretend request decryption
            request.body = request.body.replace('encrypted:', '');

            await next();

            // pretend response encryption
            response.body('encrypted:' + response._body);
          },
        ],
      },
      async (request, response) => {
        return 'hello world';
      }
    );
    const callback = jest.fn((err, data) => {});
    const event = { ...noBodyEvent, body: 'encrypted:abc' };
    // @ts-ignore
    await handler(event, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'encrypted:hello world',
    });
  });

  it('stringifies object-like', async () => {
    const handler = lambda.extend({}, async () => ({ foo: 'bar' }));
    const callback = jest.fn();

    // @ts-ignore
    await handler(noBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: JSON.stringify({ foo: 'bar' }),
    });
  });
});
