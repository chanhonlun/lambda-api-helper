import { Request } from '../../types';
import { toRequest, toResponse } from '../parser';

import context from './context.json';
import noBodyEvent from './event.no-body.json';
import textBodyEvent from './event.text-body.json';
import jsonBodyEvent from './event.json-body.json';
import formBodyEvent from './event.form-body-with-file.json';

describe('parser.ts', () => {
  describe('toRequest()', () => {
    const commonChecking = (request: Request) => {
      expect(request.method).toBe('POST');
      expect(request.path).toBe(noBodyEvent.path);
      expect(request.pathParameters).toHaveProperty('foo');
      expect(request.pathParameters.foo).toBe('bar');
      expect(request.query).toHaveProperty('foo');
      expect(request.query).toHaveProperty('baz');
      expect(request.query.foo).toBe('bar');
      expect(request.query.baz).toHaveLength(2);
      expect(request.query.baz).toContain('qux');
      expect(request.query.baz).toContain('quux');
      expect(request.headers).not.toBeNull();
      expect(request.event).not.toBeNull();
      expect(request.context).not.toBeNull();
    };

    it('parses event with no body', async () => {
      const request = toRequest(noBodyEvent, context);
      commonChecking(request);
      expect(request.body).toBeNull();
    });

    it('parses event with text body', async () => {
      const request = toRequest(textBodyEvent, context);
      commonChecking(request);
      expect(request.body).toBe(textBodyEvent.body);
    });

    it('parses event with json body', async () => {
      const request = toRequest(jsonBodyEvent, context);
      commonChecking(request);
      expect(request.body).toHaveProperty('hello');
      expect(request.body.hello).toBe('world');
    });

    it('parses event with form body', async () => {
      const request = toRequest(formBodyEvent, context);
      commonChecking(request);
      expect(request.body).toBe(formBodyEvent.body);
    });
  });

  describe('toResponse()', () => {
    it('allows to set headers', async () => {
      const callback = jest.fn((err, data) => {});
      const response = toResponse(callback);

      response.header('foo', 'bar');
      expect(response._headers).toHaveProperty('foo');
      expect(response._headers.foo).toBe('bar');
    });

    it('allows to set status', async () => {
      const callback = jest.fn((err, data) => {});
      const response = toResponse(callback);

      response.status(201);
      expect(response._status).toBe(201);
    });

    it('allows to set body', async () => {
      const callback = jest.fn((err, data) => {});
      const response = toResponse(callback);

      response.body('hello world');
      expect(response._body).toBe('hello world');
    });

    describe('send()', () => {
      it('calls original callback', async () => {
        const callback = jest.fn((err, data) => {});
        const response = toResponse(callback);

        response.send();

        expect(response.sent).toBeTruthy();
        expect(callback).toBeCalled();
        expect(callback).toBeCalledTimes(1);
        expect(callback).toBeCalledWith(null, {
          statusCode: 200,
          body: null,
        });
      });

      it('calls original callback with self property', async () => {
        const callback = jest.fn((err, data) => {});
        const response = toResponse(callback);

        response.header('foo', 'bar');
        response.status(201);
        response.body('not this content');
        response.send('hello world');

        expect(response.sent).toBeTruthy();
        expect(callback).toBeCalled();
        expect(callback).toBeCalledTimes(1);
        expect(callback).toBeCalledWith(null, {
          headers: {
            foo: 'bar',
          },
          statusCode: 201,
          body: 'hello world',
        });
      });

      it('throws error if multiple send() called', async () => {
        const callback = jest.fn((err, data) => {});
        const response = toResponse(callback);

        response.header('foo', 'bar');
        response.status(201);
        response.body('not this content');
        response.send('hello world');
        expect(() => response.send('not this content')).toThrow();

        expect(callback).toBeCalled();
        expect(callback).toBeCalledTimes(1);
        expect(callback).toBeCalledWith(null, {
          headers: {
            foo: 'bar',
          },
          statusCode: 201,
          body: 'hello world',
        });
      });
    });
  });
});
