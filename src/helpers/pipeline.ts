export type Next = () => Promise<void> | void;

export type Task<Arg> = (arg: Arg, next: Next) => Promise<void> | void;

export class Pipeline<Arg> {
  constructor(private tasks: Task<Arg>[]) {}

  push(...tasks: Task<Arg>[]) {
    this.tasks.push(...tasks);
  }

  async execute(arg: Arg) {
    let prevIndex = -1;

    const runner = async (index: number = 0): Promise<void> => {
      if (index <= prevIndex) {
        throw new Error('next() called multiple times');
      }

      prevIndex = index;

      const task = this.tasks[index];

      task && (await task(arg, () => runner(index + 1)));
    };

    await runner();
  }
}
