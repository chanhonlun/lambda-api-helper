import { Next } from '../helpers/pipeline';

export interface Request {
  method: any;
  path: string;
  body: string | any;
  pathParameters: any;
  query: any;
  headers: any;
  locals: any;
  getHeader: (path: string) => string;

  // aws original fields
  event: any;
  context: any;
}

export interface Response {
  _headers: any;
  _status: number;
  _body: any;
  sent: boolean;

  header: (key: string, value: string) => Response;
  status: (status: number) => Response;
  body: (body: any) => Response;

  send: (body?: any) => void;

  // aws original fields
  callback: any;
}

export interface HttpContext {
  request: Request;
  response: Response;
}

export { Next };

export type Middleware = (
  request: Request,
  response: Response,
  next: Next
) => Promise<void> | void;
