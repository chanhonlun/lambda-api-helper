import { APIGatewayEvent, Handler as LambdaHandler } from 'aws-lambda';

import { lambda } from '../helpers';

let _commonOptions: lambda.HandlerOptions = {};

export const commons = {
  configure(options: lambda.HandlerOptions) {
    _commonOptions = options;

    // common initialization
    _commonOptions?.initialize && _commonOptions?.initialize();
  },
};

export const baseExtend = lambda.extend;

export function extend(
  handler: lambda.HandlerFn
): LambdaHandler<APIGatewayEvent>;
export function extend(
  options: lambda.HandlerOptions,
  handler: lambda.HandlerFn
): LambdaHandler<APIGatewayEvent>;
export function extend(
  optionsOrHandler: lambda.HandlerOptions | lambda.HandlerFn,
  handler?: lambda.HandlerFn
): LambdaHandler<APIGatewayEvent> {
  let options: lambda.HandlerOptions;

  if (typeof optionsOrHandler === 'function') {
    handler = optionsOrHandler;
    options = {};
  } else {
    options = optionsOrHandler;
  }

  if (!handler) {
    throw new Error('missing handler');
  }

  const handlerOptions = {
    initialize() {
      // handler specific initialization
      options?.initialize && options?.initialize();
    },
    middlewares: [
      // common middlewares
      ...(_commonOptions?.middlewares ?? []),

      // handler specific middlewares
      ...(options.middlewares ?? []),
    ],
  };

  return lambda.extend(handlerOptions, handler);
}
