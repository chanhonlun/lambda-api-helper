import 'jest-extended';

import * as lambda from '../lambda';

import context from './context.json';
import textBodyEvent from './event.text-body.json';

describe('lambda.ts', () => {
  beforeEach(() => {
    lambda.commons.configure({});
  });

  it('supports handler only signature', async () => {
    const handler = lambda.extend(async () => 'hello world');
    const callback = jest.fn();

    // @ts-ignore
    await handler(textBodyEvent, context, callback);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('supports options before handler', async () => {
    const initialize = jest.fn();
    const mainHandler = jest.fn(async () => 'hello world');

    const handler = lambda.extend({ initialize }, mainHandler);
    const callback = jest.fn();

    // @ts-ignore
    await handler(textBodyEvent, context, callback);

    expect(initialize).toBeCalled();
    expect(initialize).toBeCalledTimes(1);
    expect(initialize).toHaveBeenCalledBefore(mainHandler);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('runs common initialization before route specified one', async () => {
    const commonInitialize = jest.fn();
    const initialize = jest.fn();
    const mainHandler = jest.fn(async () => 'hello world');

    lambda.commons.configure({ initialize: commonInitialize });

    const handler = lambda.extend({ initialize }, mainHandler);
    const callback = jest.fn();

    // @ts-ignore
    await handler(textBodyEvent, context, callback);

    expect(commonInitialize).toBeCalled();
    expect(commonInitialize).toBeCalledTimes(1);
    expect(commonInitialize).toHaveBeenCalledBefore(initialize);

    expect(initialize).toBeCalled();
    expect(initialize).toBeCalledTimes(1);
    expect(initialize).toHaveBeenCalledBefore(mainHandler);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });

  it('runs common middlewares before route specified one', async () => {
    const commonMiddleware = jest.fn(async (request, response, next) => next());
    const middleware = jest.fn(async (request, response, next) => next());
    const mainHandler = jest.fn(async () => 'hello world');

    lambda.commons.configure({ middlewares: [commonMiddleware] });

    const handler = lambda.extend({ middlewares: [middleware] }, mainHandler);
    const callback = jest.fn();

    // @ts-ignore
    await handler(textBodyEvent, context, callback);

    expect(commonMiddleware).toBeCalled();
    expect(commonMiddleware).toBeCalledTimes(1);
    expect(commonMiddleware).toHaveBeenCalledBefore(middleware);

    expect(middleware).toBeCalled();
    expect(middleware).toBeCalledTimes(1);
    expect(middleware).toHaveBeenCalledBefore(mainHandler);

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
    expect(callback).toBeCalledWith(null, {
      statusCode: 200,
      body: 'hello world',
    });
  });
});
