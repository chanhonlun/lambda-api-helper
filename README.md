Simplify lambda API creation with middleware support. 

# Installation
```
npm install lambda-api-helper
```

# Usage
## Quick start
```typescript
import { lambda } from "lambda-api-helper";

export const hello = lambda.extend(
  async (request, response) => {
    return { hello: "world" };  
  }
);
``` 

## More Options
It accepts first parameter for more options, which configure each route specifically.
```typescript
export const hello = lambda.extend(
  {
    initialize() {
      // some route-specific initialization here 
    },
    middlewares: [
      async (request, response, next) => {
        // runs middleware before request         
      
        await next();
        
        // runs middleware after response
      },
    ],
  },
  async (request, response) => {
    return "Hello World";
  }
);
```

## Common configurations
It also allows configuring common initialization and middlewares globally.
```typescript
lambda.commons.configure({
  initialize() {
    // some global initializations
  },
  middlewares: [
    // some global middlewares
  ],
})
```

## Sending Response
There are 4 ways for sending a response.
1. `return` the response in the handler. The content will be automatically stringify if it is object-like. 
2. Call `response.send(content)` to immediately send your content
3. Set your `content` by `response.body(content)`, it will be collected and send after all middlewares ran
4. Call original `callback()` provided by AWS. It is located at `response.callback()` 

# Test
```
npm run test
```

# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

# License
[BSD 3-Clause](https://choosealicense.com/licenses/bsd-3-clause)
